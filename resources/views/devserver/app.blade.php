<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="icon" href="http://localhost:8080/favicon.ico">
    <title>resources</title>
  <link href="http://localhost:8080/css/main.7ea60bf7.css" rel="preload" as="style"><link href="http://localhost:8080/js/chunk-vendors.js" rel="preload" as="script"><link href="http://localhost:8080/js/main.js" rel="preload" as="script"><link href="http://localhost:8080/css/main.7ea60bf7.css" rel="stylesheet"></head>
  <body>
    <noscript>
      <strong>We're sorry but resources doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
    @inertia
    <!-- built files will be auto injected -->
  <script type="text/javascript" src="http://localhost:8080/js/chunk-vendors.js"></script><script type="text/javascript" src="http://localhost:8080/js/main.js"></script></body>
</html>
